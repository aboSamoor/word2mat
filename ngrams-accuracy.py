#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Accuracy of ngrams."""

from argparse import ArgumentParser
import logging
import sys
from io import open
from os import path
from time import time
from glob import glob
import numpy as np

__author__ = "Rami Al-Rfou"
__email__ = "rmyeid@gmail.com"

LOGFORMAT = "%(asctime).19s %(levelname)s %(filename)s: %(lineno)s %(message)s"

def read_word2vec(file_):
  data = np.array([l.split() for l in open(file_).read().splitlines()[1:]])
  keys = np.array(data[:, 0])
  vectors = np.array(data[:, 1:]).astype('float')
  word2vec = {k: v for k,v in zip(keys, vectors)}
  return word2vec

def forward(w2v, w2v_neg, X, y):
  d = int(w2v.values()[0].shape[0] ** 0.5)
  matrices = [w2v[x].reshape((d,d)) for x in X]
  F = [matrices[0]]
  for m in matrices[1:]:
    F.append(np.dot(F[-1], m))
  v3 = w2v_neg[y].reshape((d, d))
  f = np.sum(F[-1] * v3)
  return 1.0 / (1.0 + np.exp(-f))

def main(args):
  word2vec = read_word2vec(args.model)
  word2vec_neg = word2vec
  if path.isfile(args.model+".neg"):
    word2vec_neg = read_word2vec(args.model+".neg")
  test_pairs = {tuple(l.split()[:-1]): l.split()[-1] for l in open(args.test).read().splitlines()}
  inputs = sorted(test_pairs.keys())
  top = lambda pair: sorted([(forward(word2vec, word2vec_neg, pair, str(i)), str(i)) for i in range(10)], reverse=True)
  errors = filter(lambda x: top(x)[0][1] != test_pairs[x] , inputs)
  print len(inputs), len(errors), "| Error rate = ", float(len(errors)) / len(inputs)
  print "\t".join(" ".join(x) for x in errors)


if __name__ == "__main__":
  parser = ArgumentParser()
  parser.add_argument("-m", "--model", dest="model", help="Model File")
  parser.add_argument("-t", "--test", dest="test", help="Test File")
  parser.add_argument("-l", "--log", dest="log", help="log verbosity level",
                      default="INFO")
  args = parser.parse_args()
  if args.log == 'DEBUG':
    sys.excepthook = debug
  numeric_level = getattr(logging, args.log.upper(), None)
  logging.basicConfig(level=numeric_level, format=LOGFORMAT)
  main(args)

