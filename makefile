CC = gcc
#Using -Ofast instead of -O3 might result in faster code, but is supported only by newer GCC versions
CFLAGS = -lm -pthread -Ofast -march=native -Wall -funroll-loops -Wno-unused-result


BLAS = -lblas

all: word2vec word2mat word2matblas word2matdebug word2phrase distance word-analogy compute-accuracy converter

word2vec : word2vec.c
	$(CC) word2vec.c -o word2vec $(CFLAGS)

word2mat : word2mat.c
	$(CC) word2mat.c -o word2mat $(CFLAGS)

word2matdebug : word2mat.c
	$(CC) word2mat.c -o word2matdebug -g -lm -pthread

word2matblas : word2mat.c
	$(CC) word2mat.c -o word2matblas $(CFLAGS) -lblas -DCBLAS

word2phrase : word2phrase.c
	$(CC) word2phrase.c -o word2phrase $(CFLAGS)

distance : distance.c
	$(CC) distance.c -o distance $(CFLAGS)

word-analogy : word-analogy.c
	$(CC) word-analogy.c -o word-analogy $(CFLAGS)

compute-accuracy : compute-accuracy.c
	$(CC) compute-accuracy.c -o compute-accuracy $(CFLAGS)
	chmod +x *.sh

converter : convert.c
	$(CC) convert.c -o converter $(CFLAGS)

clean:
	rm -rf word2mat word2matblas word2matdebug word2vec word2phrase distance word-analogy compute-accuracy
