make
if [ ! -e text8 ]; then
  wget http://mattmahoney.net/dc/text8.zip -O text8.gz
  gzip -d text8.gz -f
fi
time ./word2matblas -train text8 -output matrices.bin  -size 64 -negative 5 -hs 0 -sample 1e-4 -threads 20 -binary 1 -iter 1

./compute-accuracy matrices.bin 30000 < questions-words.txt &> report.log
# to compute accuracy with the full vocabulary, use: ./compute-accuracy vectors.bin < questions-words.txt
