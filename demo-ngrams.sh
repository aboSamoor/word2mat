#! /bin/bash


for window in 2 3
do
# gdb --tui --args word2matdebug -train runs/corpus${window}.txt \
#           -read-model runs/init.model\
./word2matblas -train runs/corpus${window}.txt \
           -output runs/corpus${window}.model \
           -threads 1 \
           -iter 11\
           -save-vocab runs/corpus${window}.voc \
           -size 100 \
           -window ${window}\
           -negative 5 \
           -alpha 0.01 \
           -binary 0 \
           -constant-alpha 0\
           -sample 0 \
           -tied 0\
           -min-count 1 #&> runs/corpus4.log

echo ""
./ngrams-accuracy.py -m runs/corpus${window}.model -t runs/keys${window}.txt
done
